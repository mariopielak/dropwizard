package pl.mariopielak.dropwizard.data;

import pl.mariopielak.dropwizard.model.Person;

import java.util.HashMap;
import java.util.Optional;

/**
 * Created by mariopielak on 30.06.2017.
 */
public class PersonRepository {

    private final HashMap<Integer, Person> persons = new HashMap<Integer, Person>();

    public Optional<Person> get(Integer id) {
        return Optional.ofNullable(persons.get(id));
    }

    public void add(Person person) {
        persons.put(person.getId(), person);
    }
}
