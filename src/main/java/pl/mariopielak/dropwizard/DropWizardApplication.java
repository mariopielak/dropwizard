package pl.mariopielak.dropwizard;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import pl.mariopielak.dropwizard.resource.PersonResource;

/**
 * Created by mariopielak on 30.06.2017.
 */
public class DropWizardApplication extends Application<DropWizardConfiguration> {


    public static void main(String[] args) throws Exception {
        new DropWizardApplication().run(args);
    }

    public void run(DropWizardConfiguration dropWizardConfiguration, Environment environment) throws Exception {
        final PersonResource personResource = new PersonResource();
        environment.jersey().register(personResource);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void initialize(Bootstrap<DropWizardConfiguration> bootstrap) {
        super.initialize(bootstrap);
    }


}
