package pl.mariopielak.dropwizard.resource;

import com.fasterxml.jackson.databind.util.JSONPObject;
import pl.mariopielak.dropwizard.data.PersonRepository;
import pl.mariopielak.dropwizard.model.Person;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by mariopielak on 30.06.2017.
 */
@Path("/person")
@Produces(MediaType.APPLICATION_JSON)
public class PersonResource {

    private final AtomicInteger counter = new AtomicInteger(0);

    private final PersonRepository repository = new PersonRepository();

/*
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String create(@QueryParam("name") String name, @QueryParam("nickname") String nickname) {
        repository.add(new Person(counter.getAndIncrement(), name, nickname));
        return Result;
    }
*/

    @GET
    //@Timed

    @Produces(MediaType.APPLICATION_JSON)
    public Person get(@QueryParam("id") Integer id) {
        return repository.get(id).get();
    }
}
