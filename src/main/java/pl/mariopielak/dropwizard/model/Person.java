package pl.mariopielak.dropwizard.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Length;

/**
 * Created by mariopielak on 30.06.2017.
 */
public class Person {

    private Integer id;

    @Length(min = 1, max = 10)
    private String name;

    private String nickname;

    public Person() {
        // jackson desiarlization
    }

    public Person(Integer id, String name, String nickname) {
        this.id = id;
        this.name = name;
        this.nickname = nickname;
    }

    @JsonProperty
    public String getName() {
        return name;
    }

    @JsonProperty
    public String getNickname() {
        return nickname;
    }

    public Integer getId() {
        return id;
    }
}
